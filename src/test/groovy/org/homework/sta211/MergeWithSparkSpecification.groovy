package org.homework.sta211

import com.google.common.io.Files
import groovy.io.FileType
import org.apache.commons.math3.util.Pair
import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DataTypes
import org.homework.sta211.function.CountBy30Sec
import org.homework.sta211.udf.BinaryToBooleanUDF
import org.homework.sta211.udf.CatClasseAccueilUDF
import org.homework.sta211.udf.DepartementUDF
import org.homework.sta211.udf.Interval30sUDF
import org.homework.sta211.udf.SetToNoUDF
import org.homework.sta211.udf.UCIDConvertor
import spock.lang.Shared
import spock.lang.Specification

import static org.apache.spark.sql.functions.callUDF
import static org.apache.spark.sql.functions.col

class MergeWithSparkSpecification extends Specification {

    File csv = new File("/home/developper/dev/sta211/data/JeuxDeDonneesCNAM.csv")
    File skillsCSV = new File("/home/developper/dev/sta211/data/sparkResult/FM.csv")
    File wCsv = new File("/home/developper/dev/sta211/data/sparkResult")


    @Shared
    JavaSparkContext sparkContext

    @Shared
    SparkSession session

    def setupSpec() {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("MergeWithSparkSpecification")
        sparkContext = new JavaSparkContext(conf)
        session = SparkSession.builder().config(conf).getOrCreate()
    }

    def "Explore skillsCSV"() {
        when:
        Dataset<Row> skillsDataset = session.read().format("csv").option("header", "true").load(skillsCSV.getPath())

        then:
        skillsDataset.show()
    }

    def "Merge File"() {
        when:
        Dataset<Row> mainDataset = session.read().format("csv").option("header", "true").load(csv.getPath())
        Dataset<Row> skillsDataset = session.read().format("csv").option("header", "true").load(skillsCSV.getPath())

        Dataset<Row> result = mainDataset.join(skillsDataset, skillsDataset.col("AVAYA_SKILL").equalTo(mainDataset.col("DISPSPLIT")), "inner")

        then:
        result.write().mode(SaveMode.Overwrite).format("csv").option("header", "true").save(wCsv.getPath())
        result.show()
    }

    def "Comptage des lignes"() {
        when:
        Dataset<Row> mainDataset = session.read().format("csv").option("header", "true").load(csv.getPath())
        println mainDataset.count()

        then:
        println "!= null " + mainDataset.filter(col("CATEGORIE").isNotNull()).count()
    }

    def "Map/Reduce avec Spark"() {
        setup:
        File csv = new File("/home/developper/dev/sta211/data/JeuxDeDonneesCNAM.csv")
        String dirResult = "/home/developper/dev/sta211/data/sparkResult"

        when:
        Dataset<Row> mainDataset = session.read().format("csv").option("header", "true").load(csv.getPath())
        CountBy30Sec countFlatMapOperator = CountBy30Sec.builder().telTalkTimeIndex(mainDataset.schema().fieldIndex("TEL_TALKTIME")).build()
        save(mainDataset.flatMap(countFlatMapOperator, countFlatMapOperator.getEncoder())
                .groupBy(col("period"))
                .sum("cpt").withColumnRenamed("sum(cpt)","nbrAppels")
                .orderBy(col("period")), dirResult, "/cumulAppel")
        then:
        assert true
    }

    def "Préparation des données"() {
        setup:
        File csv = new File("/home/developper/dev/sta211/data/JeuxDeDonneesCNAM.csv")
        String dirResult = "/home/developper/dev/sta211/data/sparkResult"

        when:
        Dataset<Row> mainDataset = session.read().format("csv").option("header", "true").load(csv.getPath())

        session.udf().register("ucid", new UCIDConvertor(), DataTypes.StringType)
        session.udf().register("region", DepartementUDF.build(), DataTypes.StringType)
        session.udf().register("toNo", new SetToNoUDF(), DataTypes.StringType)
        session.udf().register("binaryToBoolean", new BinaryToBooleanUDF(), DataTypes.StringType)
        session.udf().register("catClasseAccueil", new CatClasseAccueilUDF(), DataTypes.StringType)
        session.udf().register("interval30s", new Interval30sUDF(), DataTypes.StringType)

        println mainDataset.schema()


        // .withColumn("TEL_UCID", callUDF("ucid", col("TEL_UCID")))
        mainDataset = mainDataset
                .drop("TEL_APPEL_TRANSFERE")
                .drop("TEL_UCID")
                .withColumn("TEL_JOUR_SEMAINE", mainDataset.col("TEL_JOUR_SEMAINE").minus(1).cast(DataTypes.IntegerType))
                .withColumn("PART_TOP_INA_NON_TRAITE", callUDF("toNo", col("PART_TOP_INA_NON_TRAITE")))
                .withColumn("PART_REGION", callUDF("region", col("PART_DEPARTEMENT")))
                .withColumn("TEL_CLASSE_METIER", callUDF("catClasseAccueil", col("TEL_CLASSE_ACCUEIL")))
                .withColumn("TEL_TALKTIME_INTERVAL", callUDF("interval30s", col("TEL_TALKTIME")))
                .withColumn("PART_FLAG_ADH_FUA", callUDF("binaryToBoolean", col("PART_FLAG_ADH_FUA")))
                .withColumn("PART_FLAG_ADH_FUR", callUDF("binaryToBoolean", col("PART_FLAG_ADH_FUR")))
                .withColumn("PART_FLAG_ADH_FAR", callUDF("binaryToBoolean", col("PART_FLAG_ADH_FAR")))
                .withColumn("PART_FLAG_ADH_AUTRES_FM", callUDF("binaryToBoolean", col("PART_FLAG_ADH_AUTRES_FM")))
                .withColumn("PART_FLAG_ADH_FM_SURCOMPLEMENTAIRE", callUDF("binaryToBoolean", col("PART_FLAG_ADH_FM_SURCOMPLEMENTAIRE")))
                .drop(col("TEL_CLASSE_ACCUEIL"))
                .drop(col("TEL_ANSHOLDTIME"))
                .drop(col("TEL_DURATION"))
                .drop(col("TEL_OTHERTIME"))
                .drop(col("PART_DEPARTEMENT"))
                .filter(col("TEL_TALKTIME").gt(30))
                .filter(col("PART_AGE").gt(15))
                .orderBy(col("TEL_TALKTIME"))
                .withColumnRenamed("PART_FLAG_ADH_FUA", "DOSSIER_PART_FLAG_ADH_FUA")
                .withColumnRenamed("PART_FLAG_ADH_FUR", "DOSSIER_PART_FLAG_ADH_FUR")
                .withColumnRenamed("PART_FLAG_ADH_FAR", "DOSSIER_PART_FLAG_ADH_FAR")
                .withColumnRenamed("PART_FLAG_ADH_AUTRES_FM", "DOSSIER_PART_FLAG_ADH_AUTRES_FM")
                .withColumnRenamed("PART_FLAG_ADH_FM_SURCOMPLEMENTAIRE", "DOSSIER_PART_FLAG_ADH_FM_SURCOMPLEMENTAIRE")
                .withColumnRenamed("PART_REGION", "DOSSIER_PART_REGION")
                .withColumnRenamed("PART_TOP_INA_NON_TRAITE", "DOSSIER_PART_TOP_INA_NON_TRAITE")
                .withColumnRenamed("PART_AGE", "DOSSIER_PART_AGE")
                .withColumnRenamed("PART_SEXE", "DOSSIER_PART_SEXE")
                .withColumnRenamed("PART_NB_ENFANTS", "DOSSIER_PART_NB_ENFANTS")
                .withColumnRenamed("PART_NB_ENFANTS_MINEURS", "DOSSIER_PART_NB_ENFANTS_MINEURS")
                .withColumnRenamed("PART_ACTIVITE", "DOSSIER_PART_ACTIVITE")
                .withColumnRenamed("PART_CATEGORIE", "DOSSIER_PART_CATEGORIE")
                .withColumnRenamed("CTADM_TYPE_INTERLOCUTEUR", "CONTACT_TYPE_INTERLOCUTEUR")
                .withColumnRenamed("CTADM_SUJET", "CONTACT_SUJET")




        Dataset<Row> classeMetierFilterDataset = mainDataset
                .groupBy(col("TEL_CLASSE_METIER"))
                .count()
                .filter(col("count")
                .gt(1000))
                .select(col("TEL_CLASSE_METIER"))
                .withColumnRenamed("TEL_CLASSE_METIER", "TMP_TEL_CLASSE_METIER")

        classeMetierFilterDataset.show()
        mainDataset = mainDataset
                .join(classeMetierFilterDataset, col("TMP_TEL_CLASSE_METIER").equalTo(col("TEL_CLASSE_METIER")))
                .drop(col("TMP_TEL_CLASSE_METIER"))

        mainDataset.show()
        save(mainDataset, dirResult, "/main")

        // Tableau de cumul
        CountBy30Sec countFlatMapOperator = CountBy30Sec.builder().telTalkTimeIndex(mainDataset.schema().fieldIndex("TEL_TALKTIME")).build()
        save(mainDataset.flatMap(countFlatMapOperator, countFlatMapOperator.getEncoder())
                .groupBy(col("period"))
                .sum("cpt").withColumnRenamed("sum(cpt)","nbrAppels")
                .orderBy(col("period")), dirResult, "/cumulAppel")

        def datasets = mainDataset.randomSplitAsList([7.0d, 3.0d] as double[], 24)
        def trainDataset = datasets[0]
        save(datasets[0], dirResult, "/train")
        save(datasets[1], dirResult, "/test")
        /*5.times {int cpt ->
            save(trainDataset.sample(true, 70.0d), dirResult, "/sample-" + cpt)
        }*/

        save(mainDataset.filter(col("TEL_CLASSE_METIER").startsWith("FM")), dirResult, "/FM")

        // Pour PART_TOP_INA_NON_TRAITE mettre à non pour les valeurs vides
        then:
        assert true
    }

    private void save(Dataset<Row> dataset, String dir, String filename) {
        dataset.repartition(1).write().mode(SaveMode.Overwrite).format("csv").option("header", "true")
                .save(dir + filename)
    }


    def "Renommage des fichiers csv SPARK"() {
        when:
        wCsv.eachFile(FileType.DIRECTORIES) { File directory ->
            directory.eachFile(FileType.FILES) { File file ->
                if (!file.getName().endsWith("csv")) {
                    return
                }
                println file
                Files.move(file, new File(wCsv, file.getParentFile().getName() + ".csv"))
            }
            directory.delete()
        }

        then:
        assert true

    }

    def "On recherche les valeurs aberrantes"() {
        setup:
        File csv = new File("/home/developper/dev/sta211/data/v6/JeuxDeDonneesCNAM_v6.csv")

        when:
        Dataset<Row> mainDataset = session.read().format("csv").option("header", "true").load(csv.getPath())
        def sub = mainDataset.filter(col("TEL_TALKTIME").gt(2000))
        sub.show()
        println sub.count()
        then:
        assert true
    }
}
