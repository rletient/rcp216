package org.homework.sta211;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.SparkSession;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Sample1 {

    public static void main(String args[]) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Sample");
        JavaSparkContext sparkContext = new JavaSparkContext(conf);
        JavaRDD<Integer> intRDD = sparkContext.parallelize(Arrays.asList(1,2,3));
        boolean isRDDEmpty= intRDD.filter(a-> a.equals(5)).isEmpty();
        System.out.println("The RDD is empty ::"+isRDDEmpty);


    }
}
