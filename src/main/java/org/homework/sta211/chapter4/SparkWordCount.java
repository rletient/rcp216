package org.homework.sta211.chapter4;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.util.Arrays;

public class SparkWordCount {

    public static void main(String[] args) {

        SparkConf conf = new SparkConf().setMaster("local").setAppName("WordCount");
        JavaSparkContext javaSparkContext = new JavaSparkContext(conf);

        JavaRDD<String> inputData = javaSparkContext.textFile("./target/classes/wordcount.txt");

        JavaPairRDD<String, Integer> counts = inputData
                .flatMap(s -> Arrays.asList(s.split(" ")).iterator())
                .mapToPair(word -> new Tuple2<>(word, 1))
                .reduceByKey((a, b) ->
                        a + b);

        counts.saveAsTextFile("counts.txt");

                /*
        JavaPairRDD<String, Integer> wordCountRDD =
                flattenPairs.reduceByKey((v1,    v2) -> v1+v2);
        wordCountRDD.saveAsTextFile("path_of_output_file");*/
    }
}
