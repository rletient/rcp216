package org.homework.sta211.udf;

import org.apache.commons.lang.StringUtils;
import org.apache.spark.sql.api.java.UDF1;

public class Interval30sUDF implements UDF1<String,String> {

    @Override
    public String call(String strValue) throws Exception {
        int value = Integer.parseInt(strValue);
        value = value - (value % 30);
        return Integer.toString(value) + "-" + Integer.toString(value + 30);
    }
}
