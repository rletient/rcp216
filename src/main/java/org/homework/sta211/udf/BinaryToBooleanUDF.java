package org.homework.sta211.udf;

import org.apache.spark.sql.api.java.UDF1;

public class BinaryToBooleanUDF implements UDF1<String,String> {

    @Override
    public String call(String value) throws Exception {
        if("0".equals(value)) {
            return "N";
        }
        return "O";
    }
}
