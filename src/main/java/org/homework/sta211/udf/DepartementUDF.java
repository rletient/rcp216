package org.homework.sta211.udf;

import com.google.common.collect.Maps;
import org.apache.spark.sql.api.java.UDF1;

import java.util.Map;

public class DepartementUDF implements UDF1<String, String> {

    private Map<String, String> mapping;

    private DepartementUDF(Map<String, String> mapping) {
        this.mapping = mapping;
    }

    @Override
    public String call(String dept) throws Exception {
        if(dept != null && mapping.containsKey(dept)) {
            return mapping.get(dept);
        }
        return "Autres";
    }

    public static DepartementUDF build() {
        Map<String, String> mapping = Maps.newConcurrentMap();
        mapping.put("01", "Auvergne-Rhône-Alpes");
        mapping.put("02", "Hauts-de-France");
        mapping.put("03", "Auvergne-Rhône-Alpes");
        mapping.put("04", "Provence-Alpes-Côte d'Azur");
        mapping.put("05", "Provence-Alpes-Côte d'Azur");
        mapping.put("06", "Provence-Alpes-Côte d'Azur");
        mapping.put("07", "Auvergne-Rhône-Alpes");
        mapping.put("08", "Grand Est");
        mapping.put("09", "Occitanie");
        mapping.put("10", "Grand Est");
        mapping.put("11", "Occitanie");
        mapping.put("12", "Occitanie");
        mapping.put("13", "Provence-Alpes-Côte d'Azur");
        mapping.put("14", "Normandie");
        mapping.put("15", "Auvergne-Rhône-Alpes");
        mapping.put("16", "Nouvelle-Aquitaine");
        mapping.put("17", "Nouvelle-Aquitaine");
        mapping.put("18", "Centre-Val de Loire");
        mapping.put("19", "Nouvelle-Aquitaine");
        mapping.put("20", "Corse");
        mapping.put("21", "Bourgogne-Franche-Comté");
        mapping.put("22", "Bretagne");
        mapping.put("23", "Nouvelle-Aquitaine");
        mapping.put("24", "Nouvelle-Aquitaine");
        mapping.put("25", "Bourgogne-Franche-Comté");
        mapping.put("26", "Auvergne-Rhône-Alpes");
        mapping.put("27", "Normandie");
        mapping.put("28", "Centre-Val de Loire");
        mapping.put("29", "Bretagne");
        mapping.put("30", "Occitanie");
        mapping.put("31", "Occitanie");
        mapping.put("32", "Occitanie");
        mapping.put("33", "Nouvelle-Aquitaine");
        mapping.put("34", "Occitanie");
        mapping.put("35", "Bretagne");
        mapping.put("36", "Centre-Val de Loire");
        mapping.put("37", "Centre-Val de Loire");
        mapping.put("38", "Auvergne-Rhône-Alpes");
        mapping.put("39", "Bourgogne-Franche-Comté");
        mapping.put("40", "Nouvelle-Aquitaine");
        mapping.put("41", "Centre-Val de Loire");
        mapping.put("42", "Auvergne-Rhône-Alpes");
        mapping.put("43", "Auvergne-Rhône-Alpes");
        mapping.put("44", "Pays de la Loire");
        mapping.put("45", "Centre-Val de Loire");
        mapping.put("46", "Occitanie");
        mapping.put("47", "Nouvelle-Aquitaine");
        mapping.put("48", "Occitanie");
        mapping.put("49", "Pays de la Loire");
        mapping.put("50", "Normandie");
        mapping.put("51", "Grand Est");
        mapping.put("52", "Grand Est");
        mapping.put("53", "Pays de la Loire");
        mapping.put("54", "Grand Est");
        mapping.put("55", "Grand Est");
        mapping.put("56", "Bretagne");
        mapping.put("57", "Grand Est");
        mapping.put("58", "Bourgogne-Franche-Comté");
        mapping.put("59", "Hauts-de-France");
        mapping.put("60", "Hauts-de-France");
        mapping.put("61", "Normandie");
        mapping.put("62", "Hauts-de-France");
        mapping.put("63", "Auvergne-Rhône-Alpes");
        mapping.put("64", "Nouvelle-Aquitaine");
        mapping.put("65", "Occitanie");
        mapping.put("66", "Occitanie");
        mapping.put("67", "Grand Est");
        mapping.put("68", "Grand Est");
        mapping.put("69", "Auvergne-Rhône-Alpes");
        mapping.put("70", "Bourgogne-Franche-Comté");
        mapping.put("71", "Bourgogne-Franche-Comté");
        mapping.put("72", "Pays de la Loire");
        mapping.put("73", "Auvergne-Rhône-Alpes");
        mapping.put("74", "Auvergne-Rhône-Alpes");
        mapping.put("75", "Ile-de-France");
        mapping.put("76", "Normandie");
        mapping.put("77", "Ile-de-France");
        mapping.put("78", "Ile-de-France");
        mapping.put("79", "Nouvelle-Aquitaine");
        mapping.put("80", "Hauts-de-France");
        mapping.put("81", "Occitanie");
        mapping.put("82", "Occitanie");
        mapping.put("83", "Provence-Alpes-Côte d'Azur");
        mapping.put("84", "Provence-Alpes-Côte d'Azur");
        mapping.put("85", "Pays de la Loire");
        mapping.put("86", "Nouvelle-Aquitaine");
        mapping.put("87", "Nouvelle-Aquitaine");
        mapping.put("88", "Grand Est");
        mapping.put("89", "Bourgogne-Franche-Comté");
        mapping.put("90", "Bourgogne-Franche-Comté");
        mapping.put("91", "Ile-de-France");
        mapping.put("92", "Ile-de-France");
        mapping.put("93", "Ile-de-France");
        mapping.put("94", "Ile-de-France");
        mapping.put("95", "Ile-de-France");

        return new DepartementUDF(mapping);
    }
}
