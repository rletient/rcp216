package org.homework.sta211.udf;

import org.apache.spark.sql.api.java.UDF1;

public class UCIDConvertor implements UDF1<String,String> {

    @Override
    public String call(String integer) throws Exception {
        return "UCID-" + integer;
    }
}
