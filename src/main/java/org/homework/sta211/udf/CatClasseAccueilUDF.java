package org.homework.sta211.udf;

import org.apache.spark.sql.api.java.UDF1;

public class CatClasseAccueilUDF implements UDF1<String,String> {

    @Override
    public String call(String value) throws Exception {
        if(value.startsWith("FM")) {
            return "FM";
        }
        return value.substring(0,3);
    }
}
