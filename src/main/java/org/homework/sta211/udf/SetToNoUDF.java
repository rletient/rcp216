package org.homework.sta211.udf;

import org.apache.commons.lang.StringUtils;
import org.apache.spark.sql.api.java.UDF1;

public class SetToNoUDF implements UDF1<String,String> {

    @Override
    public String call(String value) throws Exception {
        if(StringUtils.isEmpty(value)) {
            return "N";
        }
        return value;
    }
}
