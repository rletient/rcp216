package org.homework.sta211.function;

import com.google.common.collect.Lists;
import org.apache.commons.math3.util.Pair;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.Iterator;
import java.util.List;

public class CountBy30Sec implements FlatMapFunction<Row, Row> {

    private StructType schema;
    private int telTalkTimeIndex;

    private CountBy30Sec() {
    }

    public Encoder<Row> getEncoder() {
        return RowEncoder.apply(schema);
    }

    public Iterator<Row> call(Row row) throws Exception {
        int talkTime = Integer.parseInt(row.getString(telTalkTimeIndex));
        int nbIte = talkTime / 30;
        List<Row> rows = Lists.newArrayList();
        for(int i = 1; i <= nbIte ; i++) {
            rows.add(RowFactory.create(i*30, 1, talkTime));
        }
        return rows.iterator();
    }

    public static Builder builder() {
        return new Builder();
    }


    public static class Builder {
        private int telTalkTimeIndex;

        public Builder telTalkTimeIndex(int telTalkTimeIndex) {
            this.telTalkTimeIndex = telTalkTimeIndex;
            return this;
        }

        public CountBy30Sec build() {
            StructType structType = new StructType();
            structType = structType.add("period", DataTypes.IntegerType, false);
            structType = structType.add("cpt", DataTypes.IntegerType, false);
            structType = structType.add("talkTime", DataTypes.IntegerType, false);
            CountBy30Sec count = new CountBy30Sec();
            count.schema = structType;
            count.telTalkTimeIndex = this.telTalkTimeIndex;
            return count;
        }
    }

}
