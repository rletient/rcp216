package org.homework.sta211.java;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class ClosureDemo {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        Function<Integer, Integer> closure = ClosureExample.closure();
        list.stream().map(closure).forEach(System.out::println);


        //flatMap Example
        Stream<List<String>> streamList = Stream.of(
                Arrays.asList("FistList-FirstElement"),
                Arrays.asList("SecondList-FirstElement", "SecondList-SecondElement"),
                Arrays.asList("ThirdList-FirstElement"));
        //The streamList is of the form List<String>
        Stream<String> flatStream = streamList
                .flatMap(strList -> strList.stream());
        // But after applying flatMap operaton it translates into  Strem<String>
        flatStream.forEach(System.out::println);
    }
}
