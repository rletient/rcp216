package org.homework.sta211.java;

import java.util.function.Function;

public class ClosureExample {

    public static Function<Integer, Integer> closure() {
        int a=3;
        return t->{
            return t*a; // a is available to be accessed in this function
        };
    }

}
