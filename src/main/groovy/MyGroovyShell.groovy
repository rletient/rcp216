class MyGroovyShell {

    static void main(String[] args) {
        def sharedData = new Binding()
        def shell = new GroovyShell(sharedData)
        def now = new Date()
        sharedData.setProperty('text', 'I am shared data!')
        sharedData.setProperty('date', now)

        println "Entrez une commande : "
        while(true) {
            Scanner scanner = new Scanner(System.in)
            def command = scanner.nextLine()
            shell.evaluate command
        }

    }
}
